package vn.com.vng.library.ibeacon.request;

import org.json.JSONObject;

import vn.com.vng.library.ibeacon.UBeacon;
import vn.com.vng.library.ibeacon.model.AdsData;
import vn.com.vng.library.ibeacon.utils.DebugUtil;


/**
 * Created by AnhHieu on 5/28/15.
 */
public class NearBeaconRequest extends AbstractRequestEx {

    String beaconId;
    String appId;
    String deviceIdentify;

    public NearBeaconRequest(String beaconId, String appId, String deviceIdentify) {
        this.beaconId = beaconId;
        this.appId = appId;
        this.deviceIdentify = deviceIdentify;
    }


    @Override
    public void buildParams() {
        addParam("beaconId", beaconId);
        addParam("appId", appId);
        addParam("deviceIdentify", deviceIdentify);
        addParam("method", "beacon_nearbeacon");
    }

    @Override
    public Object parseData(String data) throws Exception {
        JSONObject daResp = new JSONObject(data);
        AdsData ret = new AdsData();
        DebugUtil.d("NearBeaconRequest ", "parseData");
        UBeacon.isMainThread();
        ret.parse(daResp);
        return ret;
    }
}
