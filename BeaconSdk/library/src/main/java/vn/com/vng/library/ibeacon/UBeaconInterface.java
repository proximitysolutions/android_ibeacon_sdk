package vn.com.vng.library.ibeacon;

/**
 * Created by AnhHieu on 6/8/15.
 */
public interface UBeaconInterface {
    public void lock();
    public void unlock();
}
