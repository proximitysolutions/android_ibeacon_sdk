package vn.com.vng.library.ibeacon;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.Identifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.powersave.BackgroundPowerSaver;
import org.altbeacon.beacon.startup.BootstrapNotifier;
import org.altbeacon.beacon.startup.RegionBootstrap;


import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Pattern;

import vn.com.vng.library.ibeacon.model.AdsData;
import vn.com.vng.library.ibeacon.model.UBeaconData;
import vn.com.vng.library.ibeacon.request.GetListBeaconByLocationRequest;
import vn.com.vng.library.ibeacon.request.NearBeaconRequest;
import vn.com.vng.library.ibeacon.utils.DebugUtil;
import vn.com.vng.multithreadactions.ActionCallback;

import static vn.com.vng.library.ibeacon.utils.AndroidUtil.*;

/**
 * Created by AnhHieu on 5/28/15.
 */
class UBeaconClient implements
        //BeaconConsumer,
        BootstrapNotifier,
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    List<UBeaconData> mMonitorBeaconList;
    private long appId;
    private final String TAG = UBeaconClient.class.getSimpleName();
    private Timer timer;

    private Context context;
    private RegionBootstrap regionBootstrap;
    private BackgroundPowerSaver backgroundPowerSaver;
    BeaconManager beaconManager;
    private IBeaconListener listener;
    //private static final String BEACON_LAYOUT = "m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24";

    private String beaconLayout;
    private long intervalTime;

    public UBeaconClient(UBeaconConfig config) {
        this.context = UBeacon.getContext();
        beaconManager = org.altbeacon.beacon.BeaconManager.getInstanceForApplication(context);
        appId = config.getAppId();
        beaconLayout = config.getBeaconLayout();
        mMonitorBeaconList = new ArrayList<UBeaconData>();
        listener = config.getListener();
        if (!TextUtils.isEmpty(beaconLayout))
            beaconManager.getBeaconParsers().add(new BeaconParser().
                    setBeaconLayout(beaconLayout));
        backgroundPowerSaver = new BackgroundPowerSaver(context);
        initLocationClient();
        intervalTime = config.getIntervalTime();
    }


    public void start() {
        DebugUtil.d(TAG, "start");
        startTimer();
    }

    public void stop() {
        DebugUtil.d(TAG, "stop");
        cancelTimer();
        if (mGoogleApiClient != null) {
            if (mGoogleApiClient.isConnecting() || mGoogleApiClient.isConnected()) {
                stopLocationUpdate();
                mGoogleApiClient.disconnect();
            }
        }
        if (regionBootstrap != null) {
            regionBootstrap.disable();
            regionBootstrap = null;
        }
    }

    private GoogleApiClient mGoogleApiClient;

    private LocationRequest mLocationRequest;
    private boolean requestingLocationUpdate = true;

    private void initLocationClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationRequest = new LocationRequest();
        mLocationRequest
                .setInterval(LocationUtils.UPDATE_INTERVAL_IN_MILLISECONDS); //timer or interval
        mLocationRequest
                .setFastestInterval(LocationUtils.FAST_INTERVAL_CEILING_IN_MILLISECONDS);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);


    }

    private void cancelTimer() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
    }

    private void startTimer() {
        DebugUtil.d(TAG, "startTimer");
        cancelTimer();
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (!isGooglePlayServicesAvailable(context)) {
                    DebugUtil.e(TAG, "Google Play Services not available");
                    return;
                }
                if (mGoogleApiClient.isConnecting()) {

                } else if (mGoogleApiClient.isConnected()) {
                    // UBeacon.isMainThread();
                    UBeacon.runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            startLocationUpdate();
                        }
                    });
                } else {
                    mGoogleApiClient.connect();
                }
            }
        }, 1000, intervalTime);
    }


    private void startLocationUpdate() {
        DebugUtil.d(TAG, "startLocationUpdate");
        if (!isGPSEnable(context)) {
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void stopLocationUpdate() {
        DebugUtil.d(TAG, "stopLocationUpdate");
        LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);//stopping the update location
    }

    private Location bestLastKnownLocation(float minAccuracy, long minTime) {
        Location bestResult = null;
        float bestAccuracy = Float.MAX_VALUE;
        long bestTime = Long.MIN_VALUE;

        // Get the best most recent location currently available
        Location mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mCurrentLocation != null) {
            float accuracy = mCurrentLocation.getAccuracy();
            long time = mCurrentLocation.getTime();

            if (accuracy < bestAccuracy) {
                bestResult = mCurrentLocation;
                bestAccuracy = accuracy;
                bestTime = time;
            }
        }

        // Return best reading or null
        if (bestAccuracy > minAccuracy || bestTime < minTime) {
            return null;
        } else {
            return bestResult;
        }
    }

  /*  private Location getLocation() {
        Location lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        return lastKnownLocation;
    }
*/

    /* Altbeacon.startMonitoringForRegion(region)

     Start monitoring a region. Will trigger events 'enteredRegion', 'exitedRegion', and 'determinedRegionState'.

     Structure of a region:

     {
         identifier: 'Some arbitrary ID',
                 uuid: '00000000-0000-0000-0000-000000000000',
             major: INT or null,
             minor: INT or null
     }*/
    //  ity﹕ 00:02:72:CC:8A:AA  533 01122334-4556-6778-899a-abbccddeeff0 -1

    // Region test = new Region("background", Identifier.parse("01122334-4556-6778-899a-abbccddeeff0"), null, null);
    Pattern UUID_PATTERN = Pattern.compile("^[0-9A-Fa-f]{8}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{4}-?[0-9A-Fa-f]{12}$");

    private void startMonitorBeacon(List<UBeaconData> values) {
        if (listener != null) {
            listener.onNearBeacons(new ArrayList<UBeaconData>(values));
        }
        List<Region> listBeaconMonitor = new ArrayList<Region>();
        if (regionBootstrap != null) {
            regionBootstrap.disable();
            regionBootstrap = null;
        }
        for (UBeaconData ubeacon : values) {
            String uuid = ubeacon.getUuid();
            if (!UUID_PATTERN.matcher(uuid).matches()) {
                continue;
            }
            Region _region = new Region(ubeacon.getBeaconId() + "",
                    Identifier.parse(uuid),
                    Identifier.fromInt(ubeacon.getbMajor()),
                    Identifier.fromInt(ubeacon.getbMinor()));
            listBeaconMonitor.add(_region);
            DebugUtil.d(TAG, "beacon uuid | " + uuid);
        }

        DebugUtil.d(TAG, "beacon size: " + listBeaconMonitor.size());
        if (listBeaconMonitor.size() > 0)
            regionBootstrap = new RegionBootstrap(this, listBeaconMonitor);
    }

    @Override
    public Context getApplicationContext() {
        DebugUtil.d(TAG, "getApplicationContext");
        return context;
    }


    @Override
    public void didEnterRegion(final Region region) {
        DebugUtil.d(TAG, "did enter region.");
        DebugUtil.d(TAG, "uniqueId " + region.getUniqueId());
        UBeacon.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, "didEnterRegion " + region.getId1(), Toast.LENGTH_LONG).show();
            }
        });
        doRequestNearBeacon(region.getUniqueId(), "deviceIdentify");
    }

    @Override
    public void didExitRegion(final Region region) {
        DebugUtil.d(TAG, "I no longer see a beacon.");
        DebugUtil.d(TAG, "uniqueId " + region.getUniqueId());
        if (DebugUtil.isDebug)
            UBeacon.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, " didExitRegion " + region.getId1(), Toast.LENGTH_LONG).show();
                }
            });
    }

    @Override
    public void didDetermineStateForRegion(final int state, final Region region) {
        DebugUtil.d(TAG, "I have just switched from seeing/not seeing beacons: " + state);
        DebugUtil.d(TAG, "uniqueId " + region.getUniqueId());
        if (DebugUtil.isDebug)
            UBeacon.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(context, " didDetermineStateForRegion " + region.getId1() + " state " + state, Toast.LENGTH_LONG).show();
                }
            });
    }


    @Override
    public void onConnected(Bundle bundle) {
        DebugUtil.d(TAG, "onConnected Google Api");

        startLocationUpdate();
    }

    @Override
    public void onConnectionSuspended(int i) {
        DebugUtil.d(TAG, "onConnectionSuspended Google Api");
    }

    @Override
    public void onLocationChanged(Location location) {

        double latitude = location.getLatitude();
        double longitude = location.getLongitude();

        DebugUtil.d(TAG, "onLocationChanged " + latitude + " " + longitude);

        doRequestGetBeacon(longitude, latitude);


        //stop google api+ request update
        stopLocationUpdate();
        //mGoogleApiClient.disconnect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        DebugUtil.d(TAG, "onConnectionFailed Google Api");
//        if (connectionResult.hasResolution()) {
//            try {
//
//                connectionResult.startResolutionForResult(context,
//                        LocationUtils.CONNECTION_FAILURE_RESOLUTION_REQUEST);
//
//            } catch (IntentSender.SendIntentException e) {
//            }
//        } else {
//            //showErrorDialog(connectionResult.getErrorCode());
//        }
    }


    private void doRequestGetBeacon(double longitude, double latitude) {
        GetListBeaconByLocationRequest request = new GetListBeaconByLocationRequest(longitude, latitude, appId + "");
        request.start(GetBeaconCallback);
    }

    private void doRequestNearBeacon(String beaconId, String deviceIdentify) {
        NearBeaconRequest request = new NearBeaconRequest(beaconId, appId + "", deviceIdentify);
        request.start(NearBeaconCallback);
    }

    private ActionCallback GetBeaconCallback = new ActionCallback(context, 0) {
        @Override
        public void onActionCompleted(Object o) {
            if (o instanceof List) {
                DebugUtil.d(TAG, " number beacon " + ((List) o).size());
                mMonitorBeaconList = (List<UBeaconData>) o;
                startMonitorBeacon((List<UBeaconData>) o);
            }
        }

        @Override
        public void onActionFailed(Exception error) {
            DebugUtil.e(TAG, "onActionFailed " + error.getMessage());
        }

        @Override
        public void onActionSkipped() {

        }
    };

    private ActionCallback NearBeaconCallback = new ActionCallback(context, 1) {
        @Override
        public void onActionCompleted(Object o) {
            if (o instanceof AdsData) {
                if (listener != null) {
                    listener.onAdsChanged((AdsData) o);
                }
            }
        }

        @Override
        public void onActionFailed(Exception error) {

        }

        @Override
        public void onActionSkipped() {

        }
    };

    public final class LocationUtils {

        public static final int MILLISECONDS_PER_SECOND = 1000;

        public static final int UPDATE_INTERVAL_IN_SECONDS = 10;

        public static final int FAST_CEILING_IN_SECONDS = 5;

        public static final long UPDATE_INTERVAL_IN_MILLISECONDS = MILLISECONDS_PER_SECOND
                * UPDATE_INTERVAL_IN_SECONDS;

        public static final long FAST_INTERVAL_CEILING_IN_MILLISECONDS = MILLISECONDS_PER_SECOND
                * FAST_CEILING_IN_SECONDS;
    }


}
