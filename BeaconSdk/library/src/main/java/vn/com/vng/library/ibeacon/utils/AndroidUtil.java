package vn.com.vng.library.ibeacon.utils;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

import org.altbeacon.beacon.BleNotAvailableException;

/**
 * Created by AnhHieu on 6/11/15.
 */
public class AndroidUtil {
    private static final String TAG = AndroidUtil.class.getSimpleName();

    public static boolean isBLESupported(Context context) {
        if (!context.getPackageManager().hasSystemFeature(
                PackageManager.FEATURE_BLUETOOTH_LE)) {
            DebugUtil.e(TAG, "Device does not support Bluetooth");
            return false;
        }
        return true;
    }


    public static boolean isBluetoothEnable() {
        try {
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                DebugUtil.e(TAG, "Device does not support Bluetooth");
                return false;
            }
            boolean ret = mBluetoothAdapter.isEnabled();
            DebugUtil.d(TAG, " Bluetooth Enable | " + ret);
            return ret;
        } catch (Exception ex) {
            DebugUtil.e(TAG, "Device does not support Bluetooth");
        }
        return false;
    }


    public static boolean isGooglePlayServicesAvailable(Context context) {
        return ConnectionResult.SUCCESS == GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
    }

    public static boolean isGPSEnable(Context context) {
        final LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        boolean network_enabled = manager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        boolean ret = gps_enabled | network_enabled;
        if (!ret) {
            DebugUtil.e(TAG, "GPS not available");
        }
        return ret;
    }

    @TargetApi(18)
    public static boolean checkBlueToothAvailability(Context context){
        if (Build.VERSION.SDK_INT < 18) {
            throw new BleNotAvailableException("Bluetooth LE not supported by this device");
        } else if (!context.getPackageManager().hasSystemFeature("android.hardware.bluetooth_le")) {
            throw new BleNotAvailableException("Bluetooth LE not supported by this device");
        } else {
            return ((BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE)).getAdapter().isEnabled();
        }
    }
}
