package vn.com.vng.ibeaconsdk;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;


import vn.com.vng.ibeaconsdk.utils.DebugUtil;
import vn.com.vng.multithreadactions.MultiThreadActionsManager;
import vn.com.vng.multithreadactions.Options;

/**
 * Created by AnhHieu on 6/1/15.
 */
public class UBeaconManager {
    public static final String TAG="UBeaconManager";
    private static Context context;
    private static boolean init = false;

    public static boolean isMainThread(){
        boolean ret=  Looper.getMainLooper().getThread() == Thread.currentThread();
        DebugUtil.d(TAG," main thread : "+ret);
        return  ret;
    }
    private static void init(Context ctx) {
        if (ctx == null)
            throw new NullPointerException("Context not null");

        if (init) return;
        context = ctx;
        MultiThreadActionsManager
                .initialize((new Options()).buildDefault());
        applicationHandler = new Handler(context.getMainLooper());
        init = true;
    }

    private static volatile Handler applicationHandler = null;
    private final static Integer lock = 1;

    public static void runOnUiThread(Runnable runnable) {
        synchronized (lock) {
            applicationHandler.post(runnable);
        }
    }

    public static Context getContext() {
        return context;
    }

//    private static UBeaconManager instance;
//
//    public static UBeaconManager getInstance() {
//        if (instance == null)
//            instance = new UBeaconManager();
//        return instance;
//    }


    public static UBeaconInterface start(Context context, BeaconConfig config) {
        init(context);
        final UBeaconClient client = new UBeaconClient(config);
        client.start();


        UBeaconInterface ret = new UBeaconInterface() {
            @Override
            public void lock() {
                client.stop();
            }

            @Override
            public void unlock() {
                client.start();
            }
        };
        return ret;
    }

//    public static void stop() {
//    }

    private UBeaconManager() {
//        mMonitorBeaconList = new ArrayList<UBeaconData>();
    }

//    List<UBeaconData> mMonitorBeaconList;
//

//    public void setMonitorBeaconList(List<UBeaconData> values) {
//        mMonitorBeaconList.clear();
//        mMonitorBeaconList.addAll(values);
//    }


}
