package vn.com.vng.ibeaconsdk.demo;

import android.app.Application;
import android.content.Intent;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import vn.com.vng.ibeaconsdk.BeaconConfig;
import vn.com.vng.ibeaconsdk.IBeaconListener;
import vn.com.vng.ibeaconsdk.R;
import vn.com.vng.ibeaconsdk.UBeaconInterface;
import vn.com.vng.ibeaconsdk.UBeaconManager;
import vn.com.vng.ibeaconsdk.model.AdsData;
import vn.com.vng.ibeaconsdk.model.UBeaconData;
import vn.com.vng.ibeaconsdk.utils.DebugUtil;


/**
 * Created by AnhHieu on 6/2/15.
 */
public class MApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        ///  startService(new Intent(getApplicationContext(), DemoService.class));


        BeaconConfig.Builder builder = new BeaconConfig.Builder()
                .appId(9L)
                .interval(60 * 1000)
                .beaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24")
                .listener(new IBeaconListener() {
                    @Override
                    public void onAdsChanged(AdsData data) {
                        if (activity != null) {
                            DebugUtil.d("NearBeaconRequest ", "onAdsChanged");
                            UBeaconManager.isMainThread();
                            EditText et = (EditText) activity.findViewById(R.id.et_log);
                            et.append("\n+"+getCurrentTime() +": beaconId | " + data.getBeaconId() + " : " + data.getLastestAds());
                        }
                    }

                    @Override
                    public void onNearBeacons(List<UBeaconData> data) {
                        if (activity != null) {
                            EditText et = (EditText) activity.findViewById(R.id.et_log);
                            //  et.setText(data.toArray().toString());
                      }
                    }
                });
        uBeaconInterface = UBeaconManager.start(getApplicationContext(), builder.build());
    }

    private String getCurrentTime()
    {
        SimpleDateFormat format=new SimpleDateFormat("HH:mm:ss");
       // format.format((new Date())).toString();
      //  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS").format(new Date())
        return format.format((new Date())).toString();
    }
    UBeaconInterface uBeaconInterface;

    public void stop() {
        if (uBeaconInterface != null)
            uBeaconInterface.lock();
    }

    public void start() {
        if (uBeaconInterface != null)
            uBeaconInterface.unlock();
    }

    private MainActivity activity;

    public void setActivity(MainActivity activity) {
        this.activity = activity;
    }


}
