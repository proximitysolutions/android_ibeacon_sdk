package vn.com.vng.ibeaconsdk.request;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import vn.com.vng.ibeaconsdk.model.UBeaconData;

/**
 * Created by AnhHieu on 5/28/15.
 */
public class GetListBeaconByLocationRequest extends AbstractRequestEx {
   // http://dev.mapi2.me.zing.vn/frs/mapi2/beacon/api?method=beacon_listbeacon&appId=9&longitude=105.7966076337424&latitude=21.0144583120157
    double longitude;
    double latitude;
    String appId;

    public GetListBeaconByLocationRequest(double longitude, double latitude, String appId) {
        this.appId = appId;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    @Override
    public void buildParams() {
        addParam("longitude", longitude);
        addParam("latitude", latitude);
        addParam("appId", appId);
        addParam("method","beacon_listbeacon");
    }

    @Override
    public Object parseData(String data) throws Exception {
        JSONArray resp = new JSONArray(data);
        List<UBeaconData> ret = new ArrayList<UBeaconData>();
        for (int i = 0; i < resp.length(); i++) {
            JSONObject item = resp.optJSONObject(i);
            if (item == null)
                continue;
            ret.add(new UBeaconData(item));
        }
        return ret;
    }
}
